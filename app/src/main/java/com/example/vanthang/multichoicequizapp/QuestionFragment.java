package com.example.vanthang.multichoicequizapp;


import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanthang.multichoicequizapp.Interface.IQuestion;
import com.example.vanthang.multichoicequizapp.Model.CurrentQuestion;
import com.example.vanthang.multichoicequizapp.Model.Question;
import com.example.vanthang.multichoicequizapp.Ultil.Common;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionFragment extends Fragment implements IQuestion {

    FrameLayout layout_image;
    ProgressBar progressBar_question;
    TextView txt_question_text;
    CheckBox ckbA,ckbB,ckbC,ckbD;

    Question    question;
    int questionIndex=-1;

    public QuestionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_question, container, false);

        //get question
        questionIndex=getArguments().getInt("index",-1);
        question=Common.questionList.get(questionIndex);

        if (question !=null) {
            //view
            layout_image =(FrameLayout) view.findViewById(R.id.layout_image);
            progressBar_question = view.findViewById(R.id.progessBar_question);
            txt_question_text = view.findViewById(R.id.txt_question_text);
            ckbA = view.findViewById(R.id.ckbA);
            ckbB = view.findViewById(R.id.ckbB);
            ckbC = view.findViewById(R.id.ckbC);
            ckbD = view.findViewById(R.id.ckbD);

            if (question.isImageQuestion())
            {
                ImageView image_question = view.findViewById(R.id.img_question);
                Picasso.get()
                        .load(question.getQuestionImage())
                        .into(image_question, new Callback() {
                            @Override
                            public void onSuccess() {
                                progressBar_question.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Exception e) {
                                Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
            }
            else
                layout_image.setVisibility(View.GONE);

            //show
            txt_question_text.setText(question.getQuestionText());
            ckbA.setText(question.getAnswerA());
            ckbA.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b)
                        Common.selected_values.add(ckbA.getText().toString());
                    else
                        Common.selected_values.remove(ckbA.getText().toString());
                }
            });

            ckbB.setText(question.getAnswerB());
            ckbB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b)
                        Common.selected_values.add(ckbB.getText().toString());
                    else
                        Common.selected_values.remove(ckbB.getText().toString());
                }
            });

            ckbC.setText(question.getAnswerC());
            ckbC.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b)
                        Common.selected_values.add(ckbC.getText().toString());
                    else
                        Common.selected_values.remove(ckbC.getText().toString());
                }
            });

            ckbD.setText(question.getAnswerD());
            ckbD.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b)
                        Common.selected_values.add(ckbD.getText().toString());
                    else
                        Common.selected_values.remove(ckbD.getText().toString());
                }
            });
            
        }
        return view;
    }

    @Override
    public CurrentQuestion getSelectedAnswer() {
        //this function will return state of answer
        //right, wrong or normal
        CurrentQuestion currentQuestion=new CurrentQuestion(questionIndex,Common.ANSWER_TYPE.NO_ANSWER);//default
        StringBuilder result=new StringBuilder();
        if (Common.selected_values.size()>1)
        {
            //if mutilchoice
            //split answer to array
            //ex:   arr[0]=A
            //ex:   arr[1]=B
            Object[] arrayAnswer=Common.selected_values.toArray();
            for (int i=0;i<arrayAnswer.length;i++)
            {
                if (i<arrayAnswer.length-1)
                    result.append(new StringBuilder(((String)arrayAnswer[i]).substring(0,1)).append(","));
                else
                    result.append(new StringBuilder((String)arrayAnswer[i]).substring(0,1));
            }
        }else if (Common.selected_values.size()==1)
        {
            //if only one choice
            Object[] arrayAnswer=Common.selected_values.toArray();
            result.append((String)arrayAnswer[0]).substring(0,1);
        }
        if (question !=null)
        {
            //Compare correctanswer with user answer
            if (!TextUtils.isEmpty(result))
            {
                if (result.toString().equals(question.getCorrectAnswer()))
                    currentQuestion.setType(Common.ANSWER_TYPE.RIGHT_ANSWER);
                else
                    currentQuestion.setType(Common.ANSWER_TYPE.WRONG_ANSWER);
            }else
                currentQuestion.setType(Common.ANSWER_TYPE.NO_ANSWER);
        }else
        {
            Toast.makeText(getContext(), "Cannot get question", Toast.LENGTH_SHORT).show();
            currentQuestion.setType(Common.ANSWER_TYPE.NO_ANSWER);
        }
        Common.selected_values.clear();
        return currentQuestion;
    }

    @Override
    public void showCorrectAnswer() {
        //Bold correct answer
        //Partern: A,B
        String[] correctAnswer=question.getCorrectAnswer().split(",");
        for (String answer:correctAnswer)
        {
            if (answer.equals("A"))
            {
                ckbA.setTypeface(null,Typeface.BOLD);
                ckbA.setTextColor(Color.RED);
            }else if (answer.equals("B"))
            {
                ckbB.setTypeface(null,Typeface.BOLD);
                ckbB.setTextColor(Color.RED);
            }else if (answer.equals("C"))
            {
                ckbC.setTypeface(null,Typeface.BOLD);
                ckbC.setTextColor(Color.RED);
            }else if (answer.equals("D"))
            {
                ckbD.setTypeface(null,Typeface.BOLD);
                ckbD.setTextColor(Color.RED);
            }
        }
    }

    @Override
    public void disableAnswer() {
        ckbA.setEnabled(false);
        ckbB.setEnabled(false);
        ckbC.setEnabled(false);
        ckbD.setEnabled(false);
    }

    @Override
    public void resetQuestion() {
        //enable checkbox
        ckbA.setEnabled(true);
        ckbB.setEnabled(true);
        ckbC.setEnabled(true);
        ckbD.setEnabled(true);
        //remove all selected
        ckbA.setChecked(false);
        ckbB.setChecked(false);
        ckbC.setChecked(false);
        ckbD.setChecked(false);
        //remove all bold on text
        ckbA.setTypeface(null,Typeface.NORMAL);
        ckbA.setTextColor(Color.BLACK);
        ckbB.setTypeface(null,Typeface.NORMAL);
        ckbB.setTextColor(Color.BLACK);
        ckbC.setTypeface(null,Typeface.NORMAL);
        ckbC.setTextColor(Color.BLACK);
        ckbD.setTypeface(null,Typeface.NORMAL);
        ckbD.setTextColor(Color.BLACK);

    }
}
