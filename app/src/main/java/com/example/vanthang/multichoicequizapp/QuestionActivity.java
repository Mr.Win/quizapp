package com.example.vanthang.multichoicequizapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.example.vanthang.multichoicequizapp.Adapter.AnswerSheetAdapter;
import com.example.vanthang.multichoicequizapp.Adapter.QuestionFragmentAdapter;
import com.example.vanthang.multichoicequizapp.DBHelper.DBHelper;
import com.example.vanthang.multichoicequizapp.Model.CurrentQuestion;
import com.example.vanthang.multichoicequizapp.Ultil.Common;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

public class QuestionActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int CODE_GET_RESULT =1111 ;
    int time_play = Common.TOTAL_TIME;
    boolean isAnswerModeView = false;

    RecyclerView rw_answer_sheet;
    RecyclerView rw_answer_sheet_drawer;
    Button  btn_done_drawer;
    AnswerSheetAdapter answerSheetAdapter;
    TextView txt_right_answer, txt_timer;
    TextView txt_wrong_answer;
    ViewPager viewPager;
    TabLayout tabLayout;

    @Override
    protected void onDestroy() {
        if (Common.countDownTimer != null)
            Common.countDownTimer.cancel();
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(Common.Category_current.getName());
        setSupportActionBar(toolbar);


        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //take question
        takeQuestion();

        if (Common.questionList.size() > 0) {
            txt_right_answer = findViewById(R.id.txt_question_right);
            txt_timer = findViewById(R.id.txt_timer);

            txt_right_answer.setVisibility(View.VISIBLE);
            txt_timer.setVisibility(View.VISIBLE);

            txt_right_answer.setText(new StringBuilder(String.format("%d/%d", Common.right_answer_count, Common.questionList.size())));
            countTimer();


            //view
            rw_answer_sheet = findViewById(R.id.rw_grip_answer);
            rw_answer_sheet.hasFixedSize();
            if (Common.questionList.size() > 5)//neu lon hon 5 thi chia lam 2 cot
                rw_answer_sheet.setLayoutManager(new GridLayoutManager(this, Common.questionList.size() / 2));
            else
                rw_answer_sheet.setLayoutManager(new GridLayoutManager(this, 1));
            answerSheetAdapter = new AnswerSheetAdapter(this, Common.answerSheetList);
            rw_answer_sheet.setAdapter(answerSheetAdapter);

            viewPager = findViewById(R.id.viewpager_question);
            tabLayout = findViewById(R.id.tab_questions);

            genFragmentList();
            QuestionFragmentAdapter questionFragmentAdapter = new QuestionFragmentAdapter(getSupportFragmentManager(),
                    this, Common.fragmentList);
            viewPager.setAdapter(questionFragmentAdapter);
            tabLayout.setupWithViewPager(viewPager);

            //Event
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                int SCROLLING_RIGHT = 0;
                int SCROLLING_LEFT = 1;
                int SCROLLING_UNDETERMINED = 2;

                int currentScrollDirection = 2;

                private void setScrollingDirection(float positionOffset) {
                    if ((1 - positionOffset) >= 0.5)
                        this.currentScrollDirection = SCROLLING_RIGHT;
                    else if ((1 - positionOffset) <= 0.5)
                        this.currentScrollDirection = SCROLLING_LEFT;
                }

                private boolean isScrollDirectionUndetermined() {
                    return currentScrollDirection == SCROLLING_UNDETERMINED;
                }

                private boolean isScrollingRight() {
                    return currentScrollDirection == SCROLLING_RIGHT;
                }

                private boolean isScrollingLeft() {
                    return currentScrollDirection == SCROLLING_LEFT;
                }

                @Override
                public void onPageScrolled(int i, float v, int i1) {
                    if (isScrollDirectionUndetermined())
                        setScrollingDirection(v);
                }

                @Override
                public void onPageSelected(int i) {
                    QuestionFragment questionFragment;
                    int position = 0;
                    if (i > 0) {
                        if (isScrollingRight()) {
                            //if user scroll to right, get previous fragment to calculate result
                            questionFragment = Common.fragmentList.get(i - 1);
                            position = i - 1;
                        } else if (isScrollingLeft()) {
                            //if user scroll to left, get next fragment to calculate result
                            questionFragment = Common.fragmentList.get(i + 1);
                            position = i + 1;
                        } else
                            questionFragment = Common.fragmentList.get(position);
                    } else {
                        questionFragment = Common.fragmentList.get(0);
                        position = 0;
                    }

                    //show correct answer
                    CurrentQuestion question_state = questionFragment.getSelectedAnswer();
                    Common.answerSheetList.set(position, question_state);//set question answer for answersheet
                    answerSheetAdapter.notifyDataSetChanged();//change color

                    countCorrectAnswer();
                    txt_right_answer.setText(new StringBuilder(String.format("%d",Common.right_answer_count))
                    .append("/")
                    .append(String.format("%d",Common.questionList.size())));
                    txt_wrong_answer.setText(String.valueOf(Common.wrong_answer_count));

                    if (question_state.getType() ==Common.ANSWER_TYPE.NO_ANSWER )
                    {
                        questionFragment.showCorrectAnswer();
                        questionFragment.disableAnswer();
                    }
                }

                @Override
                public void onPageScrollStateChanged(int i) {
                    if (i == viewPager.SCROLL_STATE_IDLE)
                        this.currentScrollDirection = SCROLLING_UNDETERMINED;
                }
            });
        }


    }
    private void finishGame() {
        int position=viewPager.getCurrentItem();
        QuestionFragment questionFragment=Common.fragmentList.get(position);
        CurrentQuestion question_state = questionFragment.getSelectedAnswer();
        Common.answerSheetList.set(position, question_state);//set question answer for answersheet
        answerSheetAdapter.notifyDataSetChanged();//change color

        countCorrectAnswer();
        txt_right_answer.setText(new StringBuilder(String.format("%d",Common.right_answer_count))
                .append("/")
                .append(String.format("%d",Common.questionList.size())));
        txt_wrong_answer.setText(String.valueOf(Common.wrong_answer_count));

        if (question_state.getType() !=Common.ANSWER_TYPE.NO_ANSWER )
        {
            questionFragment.showCorrectAnswer();
            questionFragment.disableAnswer();
        }
        //we will show result
        Intent intent=new Intent(this,ResultActivity.class);
        Common.timer=Common.TOTAL_TIME-time_play;
        Common.no_answer_count=Common.questionList.size()-(Common.right_answer_count+Common.wrong_answer_count);
        Common.data_question=new StringBuilder(new Gson().toJson(Common.answerSheetList));
        
        startActivityForResult(intent,CODE_GET_RESULT);
    }
    private void countCorrectAnswer() {
        //reset variable
        Common.right_answer_count=Common.wrong_answer_count=0;
        for (CurrentQuestion item:Common.answerSheetList)
        {
            if (item.getType()==Common.ANSWER_TYPE.RIGHT_ANSWER)
                Common.right_answer_count++;
            else if (item.getType()==Common.ANSWER_TYPE.WRONG_ANSWER)
                Common.wrong_answer_count++;
        }
    }

    private void genFragmentList() {
        for (int i = 0; i < Common.questionList.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putInt("index", i);
            QuestionFragment questionFragment = new QuestionFragment();
            questionFragment.setArguments(bundle);

            Common.fragmentList.add(questionFragment);
        }
    }

    private void countTimer() {
        if (Common.countDownTimer == null) {
            Common.countDownTimer = new CountDownTimer(Common.TOTAL_TIME, 1000) {
                @Override
                public void onTick(long l) {
                    txt_timer.setText(String.format("%02d:%02d",
                            TimeUnit.MILLISECONDS.toMinutes(l),
                            TimeUnit.MILLISECONDS.toSeconds(l) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(l))));
                    time_play -= 1000;
                }

                @Override
                public void onFinish() {
                    //finish game
                    finishGame();
                }
            }.start();
        } else {
            Common.countDownTimer.cancel();
            Common.countDownTimer = new CountDownTimer(Common.TOTAL_TIME, 1000) {
                @Override
                public void onTick(long l) {
                    txt_timer.setText(String.format("%02d:%02d",
                            TimeUnit.MILLISECONDS.toMinutes(l),
                            TimeUnit.MILLISECONDS.toSeconds(l) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(l))));
                    time_play -= 1000;
                }

                @Override
                public void onFinish() {
                    //finish game
                }
            }.start();
        }
    }

    private void takeQuestion() {
        Common.questionList = DBHelper.getInstance(this).getAllQuestionsByCategory(Common.Category_current.getId());
        if (Common.questionList.size() == 0) {
            //if no question
            new MaterialStyledDialog.Builder(this)
                    .setTitle("Oh !")
                    .setIcon(R.drawable.ic_sentiment_dissatisfied_black_24dp)
                    .setDescription("We don't have any question in this " + Common.Category_current.getName() + " category")
                    .setPositiveText("OK")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                            finish();
                        }
                    }).show();

        } else {
            if (Common.answerSheetList.size() > 0)
                Common.answerSheetList.clear();
            //init answerSheet item from question
            //30 question = 30 answer sheet item
            for (int i = 0; i < Common.questionList.size(); i++) {
                Common.answerSheetList.add(new CurrentQuestion(i, Common.ANSWER_TYPE.NO_ANSWER));//Default all answer is no_answer
            }
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_wrong_answer);
        RelativeLayout relativeLayout=(RelativeLayout) item.getActionView();
        txt_wrong_answer=(TextView)relativeLayout.findViewById(R.id.txt_wrong_answer_count);
        txt_wrong_answer.setText(String.valueOf(0));
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.question, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_finish_game) {
            if (!isAnswerModeView)
            {
                new MaterialStyledDialog.Builder(this)
                        .setTitle("Finish ?")
                        .setIcon(R.drawable.ic_mood_black_24dp)
                        .setDescription("Do you really want to finish ?")
                        .setNegativeText("No")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveText("YES")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                                finishGame();
                            }
                        }).show();
            }
            else
                finishGame();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==CODE_GET_RESULT)
        {
            if (resultCode==RESULT_OK)
            {
                String action=data.getStringExtra("action");
                if (action==null||TextUtils.isEmpty(action))
                {
                    int questionNum=data.getIntExtra(Common.KEY_BACK_FROM_RESULT,-1);
                    viewPager.setCurrentItem(questionNum);

                    isAnswerModeView=true;
                    Common.countDownTimer.cancel();

                    txt_wrong_answer.setVisibility(View.GONE);
                    txt_right_answer.setVisibility(View.GONE);
                    txt_timer.setVisibility(View.GONE);
                }else {
                    if (action.equals("viewquizanswer"))
                    {
                        viewPager.setCurrentItem(0);
                        isAnswerModeView=true;
                        Common.countDownTimer.cancel();

                        txt_wrong_answer.setVisibility(View.GONE);
                        txt_right_answer.setVisibility(View.GONE);
                        txt_timer.setVisibility(View.GONE);
                        for (int i=0;i<Common.fragmentList.size();i++)
                        {
                            Common.fragmentList.get(i).showCorrectAnswer();
                            Common.fragmentList.get(i).disableAnswer();
                        }
                    }else if (action.equals("doitagain"))
                    {
                        viewPager.setCurrentItem(0);
                        isAnswerModeView=false;
                        countTimer();

                        txt_wrong_answer.setVisibility(View.VISIBLE);
                        txt_right_answer.setVisibility(View.VISIBLE);
                        txt_timer.setVisibility(View.VISIBLE);
                        for (CurrentQuestion item:Common.answerSheetList)
                            item.setType(Common.ANSWER_TYPE.NO_ANSWER);//reset all question
                        answerSheetAdapter.notifyDataSetChanged();

                        for (int i=0;i<Common.fragmentList.size();i++)
                            Common.fragmentList.get(i).resetQuestion();
                    }
                }
            }
        }
    }
}
