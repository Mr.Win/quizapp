package com.example.vanthang.multichoicequizapp.Interface;

import com.example.vanthang.multichoicequizapp.Model.CurrentQuestion;

public interface IQuestion {
    CurrentQuestion getSelectedAnswer();
    void showCorrectAnswer();//bold correct answer text
    void disableAnswer();//disable all checkbox
    void resetQuestion();//reset all function on question
}
