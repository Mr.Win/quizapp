package com.example.vanthang.multichoicequizapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vanthang.multichoicequizapp.Model.CurrentQuestion;
import com.example.vanthang.multichoicequizapp.R;
import com.example.vanthang.multichoicequizapp.Ultil.Common;

import java.util.List;

public class AnswerSheetAdapter extends RecyclerView.Adapter<AnswerSheetAdapter.ViewHolder> {
    Context context;
    List<CurrentQuestion> currentQuestions;

    public AnswerSheetAdapter(Context context, List<CurrentQuestion> currentQuestions) {
        this.context = context;
        this.currentQuestions = currentQuestions;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView=LayoutInflater.from(context).inflate(R.layout.layout_grip_answer_sheet_item,viewGroup,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder,final int i) {
        if (currentQuestions.get(i).getType() == Common.ANSWER_TYPE.RIGHT_ANSWER)
            viewHolder.question_item.setBackgroundResource(R.drawable.grip_question_right_answer);
        else if (currentQuestions.get(i).getType() == Common.ANSWER_TYPE.WRONG_ANSWER)
            viewHolder.question_item.setBackgroundResource(R.drawable.grip_question_wrong_answer);
        else
            viewHolder.question_item.setBackgroundResource(R.drawable.grip_question_no_answer);
    }

    @Override
    public int getItemCount() {
        return currentQuestions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View question_item;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            question_item=itemView.findViewById(R.id.question_item);
        }
    }
}
