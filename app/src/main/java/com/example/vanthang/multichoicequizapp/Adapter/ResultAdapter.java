package com.example.vanthang.multichoicequizapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.vanthang.multichoicequizapp.Interface.ItemClickListener;
import com.example.vanthang.multichoicequizapp.Model.CurrentQuestion;
import com.example.vanthang.multichoicequizapp.R;
import com.example.vanthang.multichoicequizapp.Ultil.Common;

import java.util.List;

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ViewHolder> {
    Context context;
    List<CurrentQuestion> currentQuestionList;

    public ResultAdapter() {
    }

    public ResultAdapter(Context context, List<CurrentQuestion> currentQuestionList) {
        this.context = context;
        this.currentQuestionList = currentQuestionList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.layout_result_item, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        Drawable img;
        viewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onclick(View view) {
                //When user click question,we will back QuestionActivity to show question
                LocalBroadcastManager.getInstance(context)
                        .sendBroadcast(new Intent(Common.KEY_BACK_FROM_RESULT).putExtra(Common.KEY_BACK_FROM_RESULT,
                                currentQuestionList.get(i).getQuestionIndex()));
            }
        });
        viewHolder.btn_question.setText(new StringBuilder("Question").append(currentQuestionList.get(i).getQuestionIndex() + 1));
        if (currentQuestionList.get(i).getType() == Common.ANSWER_TYPE.RIGHT_ANSWER) {
            viewHolder.btn_question.setBackgroundColor(Color.parseColor("#ff99cc00"));
            img=context.getResources().getDrawable(R.drawable.ic_check_white_24dp);
            viewHolder.btn_question.setCompoundDrawablesWithIntrinsicBounds(null,null,null,img);
        }
        else if (currentQuestionList.get(i).getType() == Common.ANSWER_TYPE.WRONG_ANSWER) {
            viewHolder.btn_question.setBackgroundColor(Color.parseColor("#ffcc0000"));
            img=context.getResources().getDrawable(R.drawable.ic_clear_white_24dp);
            viewHolder.btn_question.setCompoundDrawablesWithIntrinsicBounds(null,null,null,img);
        }
        else {
            img=context.getResources().getDrawable(R.drawable.ic_error_outline_white_24dp);
            viewHolder.btn_question.setCompoundDrawablesWithIntrinsicBounds(null,null,null,img);
        }
    }

    @Override
    public int getItemCount() {
        return currentQuestionList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Button btn_question;
        ItemClickListener itemClickListener;

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            btn_question = itemView.findViewById(R.id.btn_question);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            this.itemClickListener.onclick(view);
        }
    }
}
