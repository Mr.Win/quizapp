package com.example.vanthang.multichoicequizapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;

import com.example.vanthang.multichoicequizapp.Adapter.CategoryAdapter;
import com.example.vanthang.multichoicequizapp.DBHelper.DBHelper;
import com.example.vanthang.multichoicequizapp.Ultil.SpaceDecoration;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;
    CategoryAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar=findViewById(R.id.toolbar);
        recyclerView=findViewById(R.id.rw_category);

        toolbar.setTitle("ANT Quiz");
        setSupportActionBar(toolbar);

        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        recyclerView.hasFixedSize();

        //Get screen height
        DisplayMetrics displayMetrics=new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height=displayMetrics.heightPixels/8; //Max size of item in category
        adapter=new CategoryAdapter(DBHelper.getInstance(this).getAllCategories(),MainActivity.this);
        int spaceInPixel=4;
        recyclerView.addItemDecoration(new SpaceDecoration(spaceInPixel));
        recyclerView.setAdapter(adapter);
    }
}
