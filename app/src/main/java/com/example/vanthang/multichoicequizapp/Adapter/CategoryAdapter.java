package com.example.vanthang.multichoicequizapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanthang.multichoicequizapp.Interface.ItemClickListener;
import com.example.vanthang.multichoicequizapp.Model.Category;
import com.example.vanthang.multichoicequizapp.QuestionActivity;
import com.example.vanthang.multichoicequizapp.R;
import com.example.vanthang.multichoicequizapp.Ultil.Common;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    List<Category> categoryList;
    Context context;

    public CategoryAdapter(List<Category> categoryList, Context context) {
        this.categoryList = categoryList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView=LayoutInflater.from(context).inflate(R.layout.layout_category_item,viewGroup,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txt_category_name.setText(categoryList.get(i).getName());
        viewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onclick(View view) {
                Common.Category_current=categoryList.get(i);
                context.startActivity(new Intent(context,QuestionActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt_category_name;
        ItemClickListener itemClickListener;

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_category_name=itemView.findViewById(R.id.txt_category_name);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            this.itemClickListener.onclick(view);
        }
    }
}
